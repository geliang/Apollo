package org.gl.android.utils;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;

public class AdRobot implements ActivityListener {
	Context context;

	public AdRobot(Context context) {
		this.context = context;
	}

	@Override
	public void onStop() {
		sendBackEvent();
	}

	@Override
	public void onStart() {
		sendOnclickEvent();
	}

	String touchEventXIAOMI = "sendevent /dev/input/event1 ";

	private void sendOnclickEvent() {
		// sendevent /dev/input/event1 3 57 0
		// sendevent /dev/input/event1 3 53 40
		// sendevent /dev/input/event1 3 54 66
		// sendevent /dev/input/event1 3 58 27
		// sendevent /dev/input/event1 3 48 3
		// sendevent /dev/input/event1 0 2 0
		// sendevent /dev/input/event1 0 0 0
		// sendevent /dev/input/event1 0 2 0
		// sendevent /dev/input/event1 0 0 0
		// sendevent /dev/input/event1 0 0 0
		Runtime runtime = Runtime.getRuntime();
		try {
			Log.i(tag, "sendOnclickEvent()");
			// runtime.exec(" sendevent /dev/input/event1 3 57 0"+
			// " sendevent /dev/input/event1 3 53 40"+
			// " sendevent /dev/input/event1 3 54 66"+
			// " sendevent /dev/input/event1 3 58 27"+
			// " sendevent /dev/input/event1 3 48 3"+
			// " sendevent /dev/input/event1 0 2 0"+
			// " sendevent /dev/input/event1 0 0 0"+
			// " sendevent /dev/input/event1 0 2 0"+
			// " sendevent /dev/input/event1 0 0 0"+
			// " sendevent /dev/input/event1 0 0 0");
			// runtime.exec(touchEventXIAOMI + "3 53 40 ");
			// runtime.exec(touchEventXIAOMI + "3 54 66  ");
			// runtime.exec(touchEventXIAOMI + "3 58 27  ");
			// runtime.exec(touchEventXIAOMI + "3 48 3 ");
			// runtime.exec(touchEventXIAOMI + "0 2 0 ");
			// runtime.exec(touchEventXIAOMI + "0 0 0");
			// runtime.exec(touchEventXIAOMI + "0 2 0 ");
			// runtime.exec(touchEventXIAOMI + "0 0 0 ");
			// runtime.exec(touchEventXIAOMI + "0 0 0 ");
			runtime.exec("input tap " + "68 66");
		} catch (Exception e) {
			e.printStackTrace();
			Log.w(tag, "sendBackEvent()" + e.getMessage());
		}
	}

	String tag = "AdRobot";

	void sendBackEvent() {
		Runtime runtime = Runtime.getRuntime();
		try {
			Log.i(tag, "sendBackEvent()");
			runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
		} catch (IOException e) {
			e.printStackTrace();
			Log.w(tag, "sendBackEvent()" + e.getMessage());
		}
	}

	public void clearSdcard() {
		try {
			File file = new File("/sdcard/bddownload/");
			if (file.isDirectory()) {
				File[] files = file.listFiles();
				for (File file2 : files) {
					if (!file2.delete()) {
						Log.w(tag, "clearSdcard fail()");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
