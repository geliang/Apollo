package org.gl.android.utils;

public interface ActivityListener {
	void onStop();

	void onStart();
}
