package org.gl.android.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;

/**
 * 实现 了HDTV 服务器上的版本管理接口
 * 
 * @author think
 * 
 */
public class GLApkVersionManger implements UpdateManger.ComparisonVersion {
	String host = "http://hdtv.ap01.aws.af.cm";
	String url = host + "/APKServletDownload";
	String project = "fuliduo";
	private String about;
	UpdateManger um;

	public void autoManger(Context context, String projectName) {
		um = new UpdateManger(context);
		if (!TextUtils.isEmpty(projectName)) {
			this.project = projectName;
		}
		um.checkUpdateInfo(this);
	}

	@Override
	public String configHttpUrl() {
		return url + "?name=" + project;
	}

	@Override
	public boolean comparisonVersion(String httpVersionInfo) {
		System.out.println("" + httpVersionInfo);
		int version = 0;
		JSONArray ja = null;
		try {
			try {
				ja = new JSONArray(httpVersionInfo);
			} catch (Exception e) {
				try {
					ja = new JSONArray(
							org.gl.android.utils.Des.deCrypto(httpVersionInfo));
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			if (ja == null) {
				return false;
			}
			for (int i = 0; i < ja.length(); i++) {
				String jo = ja.optJSONObject(i).optString("image");
				if (Integer.parseInt(jo) > version) {
					version = Integer.parseInt(jo);
					about = ja.optJSONObject(i).optString("about");
					url = ja.optJSONObject(i).optString("url");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return version <= um.getLoacalVersionCode() ? false : true;
	}

	@Override
	public String configNewVersionDownloadUrl() {
		return host + "/" + url;
	}

	@Override
	public String configNewVersionInfo() {
		return about;
	}

}
