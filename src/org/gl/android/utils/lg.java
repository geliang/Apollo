package org.gl.android.utils;

import android.util.Log;

public class lg {
	public static void d(Object tag, Object message) {
		if (message instanceof Exception) {
			Log.d(tag.toString(), ((Exception) message).getMessage());
		} else {
			Log.d(tag.toString(), message.toString());
		}
	};

}
