package com.andrew.apolloMod.fm;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.Handler;

import com.google.gson.Gson;

public class DouBan {
	private final String API_CHANNEL = "http://www.douban.com/j/app/radio/channels";
	private final String API = "http://douban.fm/j/app/radio/people";
	Handler handler = new Handler();

	public DouBan() {
	}

	public void getChannels(final AjaxCallBack<Channel> callback) {
		new Thread() {
			public void run() {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(API_CHANNEL);
				try {
					HttpResponse response = httpclient.execute(httppost);
					String string = EntityUtils.toString(response.getEntity());
					final Channel mChannel = new Gson().fromJson(string,
							Channel.class);
					handler.post(new Runnable() {

						@Override
						public void run() {
							callback.onSuccess(mChannel);

						}
					});

				} catch (final Exception e) {
					e.printStackTrace();
					handler.post(new Runnable() {
						@Override
						public void run() {
							callback.onFailure(e.getCause(), 0, "");

						}
					});

				}
			};
		}.start();

	}

	/**
	 * ##### Request ```GET /j/app/radio/people```
	 * 
	 * ##### Parameters
	 * 
	 * *app_name*
	 * 
	 * App name. For Windows App, the value is ```radio_desktop_win```
	 * 
	 * *version*
	 * 
	 * Version number. For Windows App v0.97.1, the value is ```100```
	 * 
	 * 
	 * *user_id (optional)*
	 * 
	 * User id from login response
	 * 
	 * 
	 * *expire (optional)*
	 * 
	 * Expire from login response
	 * 
	 * 
	 * *token (optional)*
	 * 
	 * Token from login response
	 * 
	 * *sid*
	 * 
	 * Empty
	 * 
	 * *channel*
	 * 
	 * Channel id
	 * 
	 * *type*
	 * 
	 * Value ```n```
	 * 
	 * @param mAjaxCallBack
	 */
	public void getPlayList(String channel_id,
			AjaxCallBack<String> mAjaxCallBack) {
		FinalHttp fh = new FinalHttp();
		AjaxParams keys = new AjaxParams();
		keys.put("app_name", "radio_desktop_win");
		keys.put("version", "100");
		keys.put("channel", channel_id);
		keys.put("type", "n");
		fh.post(API, keys, mAjaxCallBack);

	}
}
