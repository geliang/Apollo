package com.andrew.apolloMod.fm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Channel {

	private List<Channel_> channels = new ArrayList<Channel_>();
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public List<Channel_> getChannels() {
		return channels;
	}

	public void setChannels(List<Channel_> channels) {
		this.channels = channels;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
